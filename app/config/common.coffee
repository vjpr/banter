module.exports = (config) ->
  appName: 'banter' # TODO
  appPrettyName: 'Trip Chat'
  port: 3030
  # TODO: Change this to the url where your site is hosted in production.
  #   See `environments/production` for usage.
  deployUrl: 'http://www.tripch.at'

  # Which database is used for the User model.
  services:
    #user: 'sequelize'
    user: 'mongoose'
