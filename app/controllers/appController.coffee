PlacesService = require('services/placesService').get()

class AppController

  @index: (req, res) =>
    res.render 'index'

  @app: (req, res) =>
    unless req.user
      res.render 'login'
    else
      res.render 'app', user: req.user

  @search: (req, res, next) ->
    PlacesService.find req.query.q, (e, r) ->
      return next e if e
      res.send r

module.exports = AppController
