logger = require('onelog').get 'CheckInController'
mongoose = require 'mongoose'
Room = mongoose.model 'Room'
_ = require 'underscore'

class CheckInController

  @new: ->

  @show: (req, res, next) ->
    logger.debug "Get whether I am checked in"
    Room.findOne {placeId: req.params.room}, (e, room) ->
      return next e if e
      if req.params.checkin is 'me'
        alreadyCheckedIn = _(room.userCheckins).find (user) -> String(user) is req.user.id
        res.send checkedIn: alreadyCheckedIn

  @index: (req, res, next) ->
    Room.findOne {placeId: req.params.room}, (e, room) ->
      return next e if e
      res.send room.userCheckins

  @update: (req, res, next) ->
    Room.findOne {placeId: req.params.room}, (e, room) ->
      return next e if e
      alreadyCheckedIn = _(room.userCheckins).find (user) -> String(user) is req.user.id
      if req.body.checkedIn
        unless alreadyCheckedIn
          room.userCheckins.push req.user.id
          room.save (e, r) ->
            return next e if e
            res.send 200
        else
          res.send 'Already checked in.'
      else
        # Remove checkin.
        room.userCheckins = _(room.userCheckins).reject (user) -> String(user) is req.user.id
        room.save (e, r) ->
          return next e if e
          res.send 200

module.exports = CheckInController
