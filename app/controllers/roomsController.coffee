logger = require('onelog').get 'RoomsController'
request = require 'request'
qs = require 'querystring'
mongoose = require 'mongoose'
_ = require 'underscore'

Room = mongoose.model 'Room'
{UserService} = require 'services/userService'
PlacesService = require('services/placesService').get()

class RoomsController

  @show: (req, res, next) ->

    done = (obj) ->
      # Sort messages by the last time they were updated or a comment was made.
      obj.messages = _(obj.messages).sortBy (item) -> 
        item.updatedAt or 0
      # Only display the 20 most recent messages.
      obj.messages = _.last obj.messages, 20
      _obj =
        room: req.params.room
        messages: []
      _.extend _obj, obj
      res.render 'room', _obj

    # Get messages from db.
    Room.findOne({placeId: req.params.room}).populate('messages.from').exec (e, room) ->
      return next e if e
      if room
        logger.debug 'Displaying room', req.params.room, room
        done room
      else
        # Create room because it doesn't exist.
        logger.debug 'Creating room', req.params.room, room
        # We need to find out the name of the place.
        unless req.query.reference?
          return next 'Place reference query parameter required.'
        PlacesService.findByReference req.query.reference, (e, details) ->
          return next e if e
          unless details?
            return next 'Place details were empty.'
          # Once we have the place name, create a room for the place.
          newRoom = 
            placeId: req.params.room
            placeName: details.result.formatted_address
            messages: []
          Room.create newRoom, (e, room) ->
            return next e if e
            done room

module.exports = RoomsController
