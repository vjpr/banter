logger = require('onelog').get 'PlacesService'
request = require 'request'
config = require('config')()
qs = require 'qs'

class @PlacesService

  @findByReference: (reference, cb) ->
    logger.debug 'Finding place by reference', reference
    params =    
      reference: reference
      sensor: true
      key: config.credentials.googlePlaces.key
    url = "https://maps.googleapis.com/maps/api/place/details/json?#{qs.stringify params}"
    # Alternate url to get maps data.
    # url = "http://maps.google.com/maps?cid=998657413562662319&q=a&output=json"
    logger.debug url
    request.get url, (e, r, b) ->
      return cb e if e
      result = JSON.parse b
      cb null, result
  
  @find: (q, cb) ->
    console.log config
    logger.debug "Searching for places by query", q
    params =
      input: q
      sensor: true
      key: config.credentials.googlePlaces.key
    request.get "https://maps.googleapis.com/maps/api/place/autocomplete/json?#{qs.stringify params}", (e, r, b) ->
      return cb e if e
      result = JSON.parse b
      result.q = q
      cb null, result

@get = ->
  # Use local dummy places service if we are working locally.
  # This is neccessary when developing without an internet connection like
  # when travelling on a bus in South America.
  env = process.env.NODE_ENV or 'development'
  if env is 'development'
    {PlacesService} = require 'services/placesServiceLocal'
  else
    {PlacesService} = require 'services/placesService'
  return PlacesService
