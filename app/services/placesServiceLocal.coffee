logger = require('onelog').get 'PlacesServiceLocal'

placeFixtures = 
  predictions:
    [
      {
        description: 'Place A'
        reference: 'a'
        id: '1'
      }
      {
        description: 'Place B'
        reference: 'b'
        id: '2'
      }
    ]

placeDetails =
  a:
    result:
      description: 'Place A'
      reference: 'a'
      id: '1' 
      formatted_address: 'Place A, Rocha, Uruguay'
  b:
    result:
      description: 'Place B'
      reference: 'a'
      id: '1' 
      formatted_address: 'Place B, New South Wales, Australia'

class @PlacesService

  # Reference can be 'a' or 'b'
  @findByReference: (reference, cb) ->
    cb null, placeDetails[reference]

  # Always returns the placeFixtures.
  @find: (q, cb) ->
    cb null, placeFixtures