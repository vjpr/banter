logger = require('onelog').get 'SocketsConnection'
_ = require 'underscore'
mongoose = require 'mongoose'
url = require 'url'
connect = require 'connect'
cookie = require 'cookie'

Room = mongoose.model 'Room'
User = mongoose.model 'User'
UserService = require 'services/userService'

# Extract the room id from the page uri.
extractPlaceIdFromUrl = (referer) ->
  pathname = url.parse(referer, true).pathname
  matches = /\/rooms\/(.*)/g.exec pathname
  # console.log 'regex', referer, matches
  if matches?
    matches[1]
  else
    null

class SocketsConnection

  constructor: (@socket) ->
    @io = @socket.manager
    hs = @socket.handshake
    if hs.session.passport.user?
      @common()
      @authenticated()
    else
      @common()
      @unauthenticated()

  common: =>
    # Client has sent a message.
    @socket.on 'msg', (msg, cb) =>
      @socket.emit 'msg', 'This is a test message'
    @socket.on 'hello', (msg, cb) =>
      logger.debug 'Socket.IO has received a message from client:', msg

  unauthenticated: =>
    hs = @socket.handshake
    logger.debug "Socket.IO has received a connection from an unauthenticated user"

  authenticated: =>
    hs = @socket.handshake
    logger.debug "Socket.IO has received a connection from user", hs.session.passport.user

    # Referer is the full uri of the page that the socket.io connection
    # was setup on.
    referer = extractPlaceIdFromUrl hs.headers.referer

    # console.log 'referer', referer
    @socket.set 'userId', hs.session.passport.user

    # Add user name to socket.
    UserService.findById hs.session.passport.user, (e, user) =>
      @socket.set 'userName', user.name
      @socket.join referer
      # Get list of participants in room.
      userList = _.map @io.sockets.clients(referer), (client) ->
        # console.log 'client', client.store.data
        id: client.store.data.userId
        name: client.store.data.userName
        disconnected: client.disconnected
      # Send userList to everyone else.
      @socket.broadcast.to(referer).emit 'userList', userList
      # Send userList to us.
      @socket.to(referer).emit 'userList', userList

    # Client has created a message.
    @socket.on 'msg', (msg, cb) =>

      # The message we broadcast must contain the populated user.
      User.findById hs.session.passport.user, (e, user) =>
        return next e if e

        placeId = referer

        # Add from field to msg.
        msg = _.extend msg,
          from: mongoose.Types.ObjectId(hs.session.passport.user)
          fromObj: name: user.name
          updatedAt: (new Date).toISOString()

        # Persist to database.
        Room.update {placeId},
          $push: messages: msg
        , (e, r, a, b) =>
          # TODO: Handle error.
          # Debug message.
          logger.debug 'Received message', msg, r, a, b
          # We need to return the id to the client to allow commenting.
          Room.findOne {placeId}, (e, r) =>
            return next e if e
            # TODO: Need to generate id on the client.
            console.log msg
            message = _(r.messages).find (persistedMsg) -> persistedMsg.cid is msg.cid

            # Broadcast to people in same room.
            @socket.broadcast.to(referer).emit 'msg', message

            cb message

    # Client has added a comment.
    @socket.on 'comment', (comment) =>

      logger.debug 'Saving comment', comment

      placeId = referer
      Room.findOne {placeId}, (e, room) =>
        #console.log 'messages', room.messages, comment.parentId
        message = _(room.messages).find (item) ->
          console.log String(item._id) is comment.parentId
          String(item._id) is comment.parentId
        #console.log 'found message', message

        User.findById hs.session.passport.user, (e, user) =>
          return next e if e

          # Add from field to msg.
          comment = _.extend comment,
            from: mongoose.Types.ObjectId(hs.session.passport.user)
            fromObj: name: user.name

          @socket.broadcast.to(referer).emit 'comment', comment

          message.updatedAt = (new Date).toISOString()

          message.comments.push comment
          room.save (e, r) ->
            return next e if e
            logger.debug 'Saved comment', comment

module.exports = SocketsConnection
