#= require jquery-1.7.2
#= require underscore/underscore
#= require backbone/backbone
#= require jade-runtime
#= require jquery-ui
#= require backbone.marionette
#= require_tree ../templates
#= require jquery.timeago
#= require jquery.autosize
#= require bootstrap/js/bootstrap-dropdown

# GUID generation.
S4 = -> return (((1+Math.random())*0x10000)|0).toString(16).substring(1)
guid = -> return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4())

# Workaround jade-runtime's use of `require`.
require = -> readFileSync: -> ''

$(document).ready ->
  console.log 'Loaded'
  # Client-side template functions are accessible as follows:
  # `JST['templates/home']()`

  router = new Router
  console.log Backbone.history.start pushState: true
  $('.timeago').timeago()

class Router extends Backbone.Router
	
  routes:
    'rooms/:id': 'room'
    'app': 'app'
  
  app: =>
    new PlaceSearchView
    resize = ->
      $('#app').height $(window).height()
    $(window).on 'resize', resize
    resize()

  room: =>
    resize = ->
      $('#main').height $(window).height()
    # $(window).on 'resize', resize
    # resize()

    new MessageBoxView collection: window.Messages
    window.Messages = new MessagesCollection window.data?.messages
    window.Messages.roomId = window.data?.room
    messagesView = new MessagesView collection: Messages
    messagesView.render()
    window.UserList = new UserListCollection
    window.UserCheckins = window.data?.userCheckins
    new UserListView collection: UserList

    checkInModel = new CheckInModel
    checkInModel.roomId = window.data?.room
    checkInView = new CheckInView model: checkInModel
    checkInModel.fetch()
    checkInView.render()

    window.socket = io.connect()
    socket.on 'msg', (msg) ->
      console.log 'received message', msg
      Messages.add msg

    # Reset userList.
    socket.on 'userList', (users) ->
      console.log 'Users', users
      UserList.reset users

    socket.on 'comment', (comment) ->
      console.log 'Got comment', comment
      parentMessage = Messages.find (msg) -> 
        msg.get('_id') is comment.parentId
      # If parent message is cached locally, we update it.
      if parentMessage?
        parentMessage.get('comments').push comment
        parentMessage.trigger 'change'

class UserCheckinsCollection extends Backbone.Collection

class CheckInModel extends Backbone.Model
  defaults:
    checkedIn: false
  isNew: => false
  url: => "/rooms/#{@roomId}/checkins/me"

class UserListCollection extends Backbone.Collection

class MessageModel extends Backbone.Model
  defaults:
    comments: []

class CommentModel extends Backbone.Model

class CommentsCollection extends Backbone.Collection
  model: CommentModel

class MessageView extends Backbone.Marionette.Layout
  
  events:
    'keydown .add-comment-box': 'onKeyDown'
    'click .show-all-comments': 'showAllComments'

  template: JST['templates/message']
  
  modelEvents:
    change: -> @render()

  regions:
    commentsRegion: '.comments'
  
  showAllComments: (e) =>
    e.preventDefault()
    @$('.show-all-comments').hide()
    coll = new CommentsCollection @model.get 'comments'
    commentsView = new CommentsView collection: coll
    @commentsRegion.show commentsView
    _.defer -> $('.timeago').timeago()

  onRender: (opts) =>
    comments = @model.get 'comments'
    comments = if comments?.length > 5 
      # Collapse comments.
      @$('.show-all-comments').show()
      @$('.show-all-comments a').html "Show all #{comments.length} comments"
      _(comments).last 5
    else
      @$('.show-all-comments').hide()
      comments
    coll = new CommentsCollection comments
    commentsView = new CommentsView collection: coll
    @commentsRegion.show commentsView
    _.defer -> $('.timeago').timeago()

  onKeyDown: (e) =>
    val = @$('.add-comment-box').val()
    if (e.keyCode is 13) and not e.shiftKey and val.length
      console.log 'Add comment!'
      comment = new CommentModel
        body: val
        createdAt: (new Date).toISOString()
        fromObj: window.data.user
        parentId: @model.get '_id'
      comments = @model.get 'comments'
      comments.push comment
      @limitComments = true
      @render()
      @$('.add-comment-box').focus()
      socket.emit 'comment', comment.toJSON()
      @$el.val ''
      e.preventDefault()
      _.defer -> $('.timeago').timeago()

class MessagesView extends Backbone.Marionette.CollectionView
  el: '#messages'
  itemView: MessageView
  appendHtml: (collectionView, itemView) ->
    collectionView.$el.prepend itemView.el

class MessagesCollection extends Backbone.Collection
  url: => "/rooms/#{@roomId}"

class UserView extends Backbone.Marionette.ItemView
  template: JST['templates/user']
  initialize: (@opts) =>
  serializeData: =>
    userIsCheckedIn = _(UserCheckins).find (userId) => @model.id is userId
    console.log 'isUserCheckedIn', userIsCheckedIn
    _.extend @model.toJSON(), {userIsCheckedIn}

class UserListView extends Backbone.Marionette.CollectionView
  el: '#user-list'
  itemView: UserView
  emptyItemView: new Backbone.Marionette.ItemView

class MessageBoxView extends Backbone.View

  el: '#message-box'

  events:
    'keydown': 'onKeyDown'

  initialize: =>
    @$el.autosize()

  onKeyDown: (e) =>
    val = @$el.val()
    if e.keyCode is 13 and not e.shiftKey and val.length
      console.log 'Sending message!'
      message = new MessageModel
        body: val
        createdAt: (new Date).toISOString()
        fromObj: window.data.user
        cid: guid()
      Messages.add message
      socket.emit 'msg', message.toJSON(), (data) ->
        console.log 'ack', data
        message.set data
      @$el.val ''
      @$el.removeAttr 'style'
      e.preventDefault()
      _.defer -> $('.timeago').timeago()

class PlaceSearchView extends Backbone.View

	el: '#place'

	events:
    'keyup': 'onKeyUp'
	
  initialize: =>
    @$el.autocomplete
      source: (req, res) ->
        $.getJSON '/search', {q: req.term}, (data, status, xhr) ->
          res _.map data.predictions, (item) ->
            label: item.description
            value: item.description
            id: item.id
            reference: item.reference
      minLength: 2
      select: (event, ui) =>
        if ui.item
          console.log "Selected: " + ui.item.value + " aka " + ui.item.id
          @$el.html "<p>Loading chat room...</p>"
          # window.location.href = "/rooms/#{ui.item.id}?placeName=#{encodeURIComponent ui.item.value}"
          window.location.href = "/rooms/#{ui.item.id}?reference=#{encodeURIComponent ui.item.reference}"
        else
          console.log "Nothing selected, input was " + this.value

  onKeyUp: =>

class CommentView extends Backbone.Marionette.ItemView
   template: JST['templates/comment']

class CommentsView extends Backbone.Marionette.CollectionView

  itemView: CommentView

  # emptyView: Backbone.Marionette.ItemView.extend
    # template: -> "<p>No comments yet</p>"

class CheckInView extends Backbone.Marionette.ItemView
  
  events:
    'click .js-check-in': 'checkIn'
    'click .js-check-out': 'checkOut'

  modelEvents:
    'change': -> @update()
  
  template: JST['templates/checkin']
  
  el: '#checkin'

  itemViewOptions: =>

  update: =>
    @render()
  
  checkIn: =>
    @model.set checkedIn: true
    @model.save()

  checkOut: =>
    @model.set checkedIn: false
    @model.save()
