Everything in this folder should be generic.

If you need to make non-generic changes make them in a separate config file or
copy one of the files into the config directory and require it from
`config/application.coffee`.
