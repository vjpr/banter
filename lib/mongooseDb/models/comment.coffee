mongoose = require 'mongoose'
Schema = mongoose.Schema

module.exports = ->

  CommentSchema = new Schema
    from: { type: Schema.Types.ObjectId, ref: 'User' }
    fromObj: Schema.Types.Mixed
    body: String
    createdAt: Date
    parentId: String

  mongoose.model 'Comment', CommentSchema
  CommentSchema
