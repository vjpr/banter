mongoose = require 'mongoose'
Schema = mongoose.Schema

module.exports = ->

  MessageSchema = new Schema
    from: { type: Schema.Types.ObjectId, ref: 'User' }
    fromObj: Schema.Types.Mixed
    body: String
    createdAt: Date
    comments: [mongoose.model('Comment').schema]
    updatedAt: Date
    cid: String

  mongoose.model 'Message', MessageSchema
  MessageSchema
