mongoose = require 'mongoose'
Schema = mongoose.Schema

module.exports = ->

  RoomSchema = new Schema
    placeId: String
    placeName: String
    messages: [mongoose.model('Message').schema]
    userCheckins: [{ type: Schema.Types.ObjectId, ref: 'User' }]

  mongoose.model 'Room', RoomSchema
  RoomSchema
